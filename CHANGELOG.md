## [1.0.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/oidc2krb/compare/v1.0.0...v1.0.1) (2024-03-08)


### Bug Fixes

* update link to git repo ([4e74f84](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/oidc2krb/commit/4e74f8422c38de99c0daac6fd174c21f5c8018d7))

# 1.0.0 (2024-02-26)


### Bug Fixes

* initial release ([8f831f0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/oidc2krb/commit/8f831f0d9a06e124a7d461cb7d1a9e5fe760f851))


### Features

* callable for uWSGI ([80a3501](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/oidc2krb/commit/80a35011d167c69f2f1fdd6029cc86d9d994ca98))
* Implemented initial version ([cc49202](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/oidc2krb/commit/cc4920286752211ab2d268f433bdf5f39a9cd23e))
