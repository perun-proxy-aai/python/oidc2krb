import logging
from typing import List

import requests
import subprocess
import time
import yaml

from flask import Flask, request
from flask_httpauth import HTTPBasicAuth
from yaml.loader import SafeLoader
from werkzeug.exceptions import BadRequest, HTTPException
from werkzeug.security import generate_password_hash, check_password_hash

app = Flask(__name__)
auth = HTTPBasicAuth()

with open("config.yaml", "r") as stream:
    config = yaml.load(stream, Loader=SafeLoader)

users = {}
if "basic-auth" in config.keys():
    ba_username = config.get("basic-auth").get("username")
    ba_password = config.get("basic-auth").get("password")
    users[ba_username] = generate_password_hash(ba_password)
    logging.info(f"Configured basic-auth for username '{ba_username}'")

if "oidc" not in config.keys():
    logging.error("No OIDC section in configuration file present")
    raise Exception("No OIDC section in configuration file present")
oidc_config = config.get("oidc")

WELL_KNOWN = oidc_config.get("well-known", "")
if len(WELL_KNOWN) < 1:
    logging.error("well-known is mandatory configuration option in oidc section")
    raise Exception("well-known is mandatory configuration option in oidc section")

# INTROSPECTION CONFIGURATION

introspection_config = oidc_config.get("introspection", {})
INTROSPECTION_ENABLED = len(introspection_config) > 0
if INTROSPECTION_ENABLED:
    INTROSPECT_CLIENT_ID = introspection_config.get("client-id", "")
    if len(INTROSPECT_CLIENT_ID) < 1:
        logging.error("client-id is mandatory configuration option")
        raise Exception("client-id is mandatory configuration option")

    INTROSPECT_CLIENT_SECRET = introspection_config.get("client-secret", "")
    if len(INTROSPECT_CLIENT_SECRET) < 1:
        logging.error("client-secret is mandatory configuration option")
        raise Exception("client-secret is mandatory configuration option")
    KRB_EXCHANGE_SCOPES = introspection_config.get("required-scopes", [])

# USERNAME CONFIGURATION
USERNAME_SCOPE = oidc_config.get("username-scope", "")
if len(USERNAME_SCOPE) < 1:
    logging.error("username is mandatory configuration option")
    raise Exception("username is mandatory configuration option")

USERNAME_CLAIM = oidc_config.get("username-claim", USERNAME_SCOPE)
if len(USERNAME_CLAIM) < 1:
    logging.error("username is mandatory configuration option")
    raise Exception("username is mandatory configuration option")

KRB_REALM = config.get("krb-realm", "")
if len(KRB_REALM) < 1:
    logging.error("username is mandatory configuration option")
    raise Exception("username is mandatory configuration option")


def fetch_endpoints_location() -> tuple:
    well_known_req = requests.get(WELL_KNOWN)
    if well_known_req.status_code != 200:
        logging.error(
            f"Could not fetch well-known configuration from '{well_known_req.url}, "
            + f"response code '{well_known_req.status_code}'"
        )
        raise Exception(
            f"Could not fetch well-known configuration from '{well_known_req.url}'"
        )
    well_know_doc = well_known_req.json()
    logging.debug("Got response from well-known endpoint")

    introspection_endpoint = well_know_doc.get("introspection_endpoint", "")
    userinfo_endpoint = well_know_doc.get("userinfo_endpoint", "")
    logging.info(f"Introspection endpoint resolved to '{introspection_endpoint}'")
    logging.info(f"Userinfo endpoint resolved to '{userinfo_endpoint}'")
    return introspection_endpoint, userinfo_endpoint


def get_username(token: str, userinfo_endpoint: str) -> str:
    logging.debug("Fetching username")
    headers = {"Authorization": "Bearer " + token}
    userinfo_req = requests.post(userinfo_endpoint, headers=headers)
    if userinfo_req.status_code != 200:
        logging.error(
            "Failed to make userinfo request, "
            f"response code '{userinfo_req.status_code}'"
        )
        raise Exception("Failed to make userinfo request")
    userinfo = userinfo_req.json()
    logging.info(f"Fetched userinfo - response: '{userinfo}'")
    username = userinfo.get(USERNAME_CLAIM, "")
    if len(username) < 1:
        logging.error(
            "Could not extract username from userinfo response "
            f"- used claim '{USERNAME_CLAIM}'"
        )
        raise Exception(f"Could not extract username from claim {USERNAME_CLAIM}")
    logging.info(f"Extracted username '{username}'")
    return username


def introspect(token: str, introspection_endpoint: str):
    if not INTROSPECTION_ENABLED:
        return

    logging.debug(f"Introspecting token '{token}'")
    introspection_req = requests.post(
        introspection_endpoint,
        data={"token": token},
        auth=(INTROSPECT_CLIENT_ID, INTROSPECT_CLIENT_SECRET),
    )
    if introspection_req.status_code != 200:
        logging.error(
            "Failed to make introspection request, "
            f"response code '{introspection_req.status_code}'"
        )
        raise Exception("Failed to make introspection request")
    introspection_result = introspection_req.json()
    logging.info(f"Introspected token - response: '{introspection_result}'")
    if not introspection_result.get("active", False):
        logging.error("Provided token has been introspected as inactive")
        raise BadRequest()
    token_scopes = introspection_result.get("scope", "")
    if USERNAME_SCOPE not in token_scopes:
        logging.error(
            "Provided token does not contain username scope for KRB ticket exchange"
            + f" - username scope '{USERNAME_SCOPE}', token scopes: '{token_scopes}'",
        )
        raise BadRequest()
    if len(KRB_EXCHANGE_SCOPES) > 0:
        for scope in KRB_EXCHANGE_SCOPES:
            if scope not in token_scopes:
                logging.error(
                    "Provided token does not contain required scope for KRB exchange"
                    f" - scope '{scope}', token scopes: '{token_scopes}'",
                )
                raise BadRequest()


def execute_krb_command(username: str) -> List[str]:
    krb_username = f"{username}@{KRB_REALM}"
    cmd = "krb525_renew"
    logging.debug(f"Executing {cmd} for user '{krb_username}")
    cmnd = subprocess.Popen(
        [cmd, krb_username], stdout=subprocess.PIPE, stderr=subprocess.STDOUT
    )
    stdout, stderr = cmnd.communicate()
    if cmnd.returncode != 0:
        logging.error(
            "Failed to execute krb525 exchange command, "
            f"original output '{stdout}', error output '{stderr}"
        )
        raise Exception("Failed to execute the krb525 exchange command")
    logging.debug("Command run successfully, constructing response")
    lines = stdout.splitlines()
    parsed_response = []
    for line in lines:
        parsed_response.append(line.decode("utf-8"))
    return parsed_response


def prepare_output(cmd_output_lines: List[str]) -> dict:
    output = {}
    for line in cmd_output_lines:
        if line.startswith("Type:"):
            output["type"] = line.replace("Type: ", "")
        elif line.startswith("Valid until:"):
            expiration = int(line.replace("Valid until: ", ""))
            output["expiration"] = expiration
            output["expires_in"] = max(expiration - int(time.time()), 0)
        else:
            output["ticket"] = line
    return output


@app.errorhandler(BadRequest)
def handle_bad_request(e: BadRequest):
    logging.error("Handling BadRequest", e)
    return e, 400


@app.errorhandler(Exception)
def handle_general_exception(e):
    if isinstance(e, HTTPException):
        logging.error("Handling HTTPException", e)
        return e
    else:
        logging.error("Handling general Exception", e)
    return "Internal server error", 500


@app.route("/", methods=["POST"])
@auth.login_required
def api_handle():
    content = request.json
    if not content:
        logging.error("Request with no body, returning BadRequest")
        raise BadRequest()
    elif "token" not in content:
        logging.error("Body with no token, returning BadRequest")
        raise BadRequest()
    token = content["token"]
    if not token or len(token.strip()) < 1:
        logging.error(f"Invalid token passed - '{token}'")
        raise BadRequest()

    introspection_endpoint, userinfo_endpoint = fetch_endpoints_location()

    introspect(token=token, introspection_endpoint=introspection_endpoint)
    username = get_username(token=token, userinfo_endpoint=userinfo_endpoint)

    cmd_output_lines = execute_krb_command(username)
    output = prepare_output(cmd_output_lines)

    logging.info(f"Token for user '{username}' exchanged, returning response")
    return output


@auth.verify_password
def verify_password(username, password):
    if users:
        if username in users and check_password_hash(users.get(username), password):
            return username
    else:
        return "anonymous_access_user"


# for uWSGI
def get_app(*args):
    return app(*args)


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=80, debug=False)
